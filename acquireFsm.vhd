library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity acquireFsm is
	port 
	(
		CLK_5MHz : in std_logic;
		RST : in std_logic;
		ARE_DATA : in std_logic;
		SWITCH : in std_logic_vector (7 downto 0);
		FIFO_FULL : in std_logic;

		WRITE_EN : out std_logic;
		WRITE_DATA : out std_logic_vector (7 downto 0)
	);
end entity acquireFsm;

architecture Behavioral of acquireFsm is
	type state_type is (idle, debounce_press, stuff_fifo, wait_for_release, debounce_release);
	signal cstate, nstate : state_type;
	
	signal sum, nsum : std_logic_vector (7 downto 0);
	signal count, ncount : natural;
	signal pulse, npulse : std_logic;

begin
	process(CLK_5MHz)
	begin
		if rising_edge(CLK_5MHz) then
			if RST = '1' then
				cstate <= idle;
				sum <= (others => '0');
				count <= 0;
			else
				cstate <= nstate;
				sum <= nsum;
				count <= ncount;
				pulse <= npulse;
				WRITE_EN <= pulse;
			end if;
		end if;
	end process;
	
	process(cstate, sum, count, ARE_DATA, pulse)
	begin
	
		case cstate is
			when idle =>
				npulse <= '0';
				ncount <= 0;
				nsum <= sum;
				if ARE_DATA = '1' then
					nstate <= debounce_press;
				else
					nstate <= idle;
				end if;
				
			when debounce_press =>
				npulse <= '0';
				ncount <= count + 1;
				nsum <= sum;
				if count = 50000 then
					nstate <= stuff_fifo;
					npulse <= '1';
				else
					nstate <= debounce_press;
				end if;
				
			when stuff_fifo =>
				ncount <= 0;
				npulse <= '0';
				nsum <= SWITCH;
				if FIFO_FULL = '0' then
					WRITE_DATA <= sum;
					nstate <= wait_for_release;
				else 
					nstate <= stuff_fifo;
				end if;
				
			when wait_for_release =>
				npulse <= '0';
				ncount <= 0;
				nsum <= sum;
				if ARE_DATA = '0' then
					nstate <= debounce_release;
				else
					nstate <= wait_for_release;
				end if;
				
			when debounce_release =>
				npulse <= '0';
				ncount <= count + 1;
				nsum <= sum;
				if count = 50000 then
					nstate <= idle;
				else
					nstate <= debounce_release;
				end if;
				
			when others =>
				npulse <= '0';
				ncount <= count;
				nsum <= sum;
				nstate <= idle;
		end case;
	end process;

end architecture;