library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity accumFsm is
    port
    (
      CLK_12p5MHz : in std_logic;
		USED_W : in std_logic_vector (3 downto 0);
		FIFO_EMPTY : in std_logic;
		READ_DATA : in std_logic_vector (7 downto 0);
		RST : in std_logic;

		READ_EN : out std_logic;
		DISP_NUM : out unsigned (23 downto 0) := (others => '0')
    );   
end entity accumFsm;

architecture Behavioral of accumFsm is

	type state_type is (reset, idle, drain_fifo, check_number, output_number);
	signal cstate, nstate : state_type;
	signal sum, nsum : unsigned (23 downto 0) := X"000000";
	signal pulse, npulse : std_logic := '0';
	signal count, ncount, delay, ndelay : natural := 0;
	
begin
	process(CLK_12p5MHz)
	begin
		if rising_edge(CLK_12p5MHz) then
			if RST = '1' then
				cstate <= reset;
				sum <= (others => '0');
			else
				cstate <= nstate;
				sum <= nsum;
				count <= ncount;
				pulse <= npulse;
				delay <= ndelay;
				READ_EN <= pulse;
			end if;
		end if;
	end process;

	process(cstate, FIFO_EMPTY, count)
	begin
		case cstate is
			when reset =>
				if RST = '1' then
					nsum <= sum;
					nstate <= reset;
				else
					nstate <= output_number;
				end if;
				
			when idle =>
				if FIFO_EMPTY = '0' then
					nstate <= drain_fifo;
					npulse <= '1';
				else
					nstate <= idle;
				end if;

			when drain_fifo =>
				npulse <= '0';
				ndelay <= delay + 1; 
				if delay > 10 then
					ndelay <= 0;
					ncount <= count + 1;
					nsum <= sum + unsigned(READ_DATA);
					nstate <= check_number;
				else
					nstate <= drain_fifo;
				end if;
			
			when check_number =>
				if count > 9 then
					nstate <= output_number;
				else
					nstate <= idle;
				end if;

			
			when output_number =>
				npulse <= '0';
				DISP_NUM <= sum;
				ncount <= 0;
				ndelay <= delay + 1; 
				if delay > 10 then
					nstate <= idle;
					ndelay <= 0;
				else
					nstate <= output_number;
				end if;
				
			when others =>
				ncount <= count;
				nsum <= sum;
				nstate <= idle;
		end case;
	end process;   
end Behavioral;