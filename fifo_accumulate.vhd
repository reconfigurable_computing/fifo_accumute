library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity fifo_accumulate is
port (
	-- Global Inputs
		MAX10_CLK1_50 : in std_logic;
		SW 	 		  : in std_logic_vector (9 downto 0);
	-- Global Outputs
		LEDR : out std_logic_vector(9 downto 0);
		HEX0 : out std_logic_vector(7 downto 0);
		HEX1 : out std_logic_vector(7 downto 0);
		HEX2 : out std_logic_vector(7 downto 0);
		HEX3 : out std_logic_vector(7 downto 0);
		HEX4 : out std_logic_vector(7 downto 0);
		HEX5 : out std_logic_vector(7 downto 0)
	);
end fifo_accumulate;

architecture Behavioral of fifo_accumulate is
	type RAM is array (0 to 15) of std_logic_vector(7 downto 0);
	constant lut : RAM := (X"C0", X"F9", X"A4", X"B0", 
						   X"99", X"92", X"82", X"F8", 
						   X"80", X"98", X"88", X"83", 
						   X"C6", X"A1", X"86", X"8E");

	signal clk_5MHz, clk_12p5MHz : std_logic;
	signal sum : unsigned (23 downto 0) := X"000000";
	signal rdclk, rdreq, wrclk, wrreq, rdempty, wrfull : std_logic;
	signal data, q : std_logic_vector (7 downto 0);
	signal rdusedw : std_logic_vector (3 downto 0);

	component pll -- PLL Component 
	port
	(
		areset		: IN STD_LOGIC  := '0';
		inclk0		: IN STD_LOGIC;
		
		c0			: OUT STD_LOGIC ; -- 5 MHz clk
		c1			: OUT STD_LOGIC  -- 12.5 MHz clk
	);
	end component;

	component fifo
	port
	(
		aclr		: IN STD_LOGIC  := '1';
		data		: IN STD_LOGIC_VECTOR (7 DOWNTO 0);
		rdclk		: IN STD_LOGIC ;
		rdreq		: IN STD_LOGIC ;
		wrclk		: IN STD_LOGIC ;
		wrreq		: IN STD_LOGIC ;

		q			: OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
		rdempty		: OUT STD_LOGIC ;
		rdusedw		: OUT STD_LOGIC_VECTOR (3 DOWNTO 0);
		wrfull		: OUT STD_LOGIC 
	);
	end component;

	component acquireFsm
	port (
		CLK_5MHz : in std_logic;
		RST : in std_logic;
		ARE_DATA : in std_logic;
		SWITCH : in std_logic_vector (7 downto 0);
		FIFO_FULL : in std_logic;
	
		WRITE_EN : out std_logic;
		WRITE_DATA : out std_logic_vector (7 downto 0)
		);
	end component;

	component accumFsm
	port 
	(
		CLK_12p5MHz : in std_logic;
		USED_W : in std_logic_vector (3 downto 0);
		FIFO_EMPTY : in std_logic;
		READ_DATA : in std_logic_vector (7 downto 0);
		RST : in std_logic;

		READ_EN : out std_logic;
		DISP_NUM : out unsigned (23 downto 0)
	);
	end component;
  						
begin
	my_pll : pll
	port map 
	(
		areset => open,
		inclk0 => MAX10_CLK1_50,

		c0 => clk_5MHz,
		c1 => clk_12p5MHz
	);

	my_fifo : fifo
	port map
	(
		aclr => SW(8),
		data => data,
		rdclk => clk_12p5MHz,
		rdreq => rdreq,
		wrclk => clk_5MHz,
		wrreq => wrreq,

		q => q,
		rdempty => rdempty,
		rdusedw	=> rdusedw,
		wrfull => wrfull
	);

	my_acquireFsm : acquireFsm
	port map
	(
		CLK_5MHz => clk_5MHz,
		RST => SW(8),
		ARE_DATA => SW(9),
		SWITCH => SW(7 downto 0),
		FIFO_FULL => wrfull,
	
		WRITE_EN => wrreq,
		WRITE_DATA => data
	);

	my_accumFsm : accumFsm
	port map
	(
		CLK_12p5MHz => clk_12p5MHz,
		USED_W => rdusedw,
		FIFO_EMPTY => rdempty,
		READ_DATA => q,
		RST => SW(8),

		READ_EN => rdreq,
		DISP_NUM => sum
	);

	HEX5 <= lut(to_integer((sum(23 downto 20))));
	HEX4 <= lut(to_integer((sum(19 downto 16))));
	HEX3 <= lut(to_integer((sum(15 downto 12))));
	HEX2 <= lut(to_integer((sum(11 downto 8))));
	HEX1 <= lut(to_integer((sum(7 downto 4))));
	HEX0 <= lut(to_integer((sum(3 downto 0))));

	LEDR <= SW;
									
end Behavioral;


